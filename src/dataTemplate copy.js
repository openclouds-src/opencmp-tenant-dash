import vmpPlatformIcon from "../src/assets/vmpPlatformIcon.png";
import cloudPlatformIcon from "../src/assets/cloudPlatformIcon.png";
import containerPlatformIcon from "../src/assets/containerPlatformIcon.png";
import physicalPlatformIcon from "../src/assets/physicalPlatformIcon.png";
import zoneIcon from "../src/assets/zoneIcon.png";
import clusterIcon from "../src/assets/clusterIcon.png";
import hostIcon from "../src/assets/hostIcon.png";
import vmIcon from "../src/assets/vmIcon.png";
import containerIcon from "../src/assets/containerIcon.png";
import tenantIcon from "../src/assets/tenantIcon.png";
import userIcon from "../src/assets/userIcon.png";
import terminalIcon from "../src/assets/terminalIcon.png";
import allTemplateIcon from "../src/assets/allTemplate.svg";
import vmTemplateIcon from "../src/assets/vmTemplate.svg";
import containerTemplateIcon from "../src/assets/containerTemplate.svg";
import serviceCatalogIcon from "../src/assets/serviceCatalogIcon.svg";
import cpuIcon from "../src/assets/cpuIcon.png";
import memoryIcon from "../src/assets/memoryIcon.png";
import storageIcon from "../src/assets/storageIcon.png";

const PLATFORM_WIDTH = 30;
const PLATFORM_HEIGHT = 6;

const RESOURCE_WIDTH = 15;
const RESOURCE_HEIGHT = 7;

const TEMPLATE_WIDTH = 90;
const TEMPLATE_HEIGHT = 6;

const SERVICECATALOG_WIDTH = 30;
const SERVICECATALOG_HEIGHT = 6;

const STATISTICSPIE_WIDTH = 30;
const STATISTICSPIE_HEIGHT = 30;

const VM_ALLOCATION_WIDTH = 60;
const VM_ALLOCATION_HEIGHT = 30;

const RESOURCE_STATISTICS_WIDTH = 40;
const RESOURCE_STATISTICS_HEIGHT = 10;

const RESOURCE_ALLOCATION_WIDTH = 40;
const RESOURCE_ALLOCATION_HEIGHT = 30;

const ARCH_OS_WIDTH = 40;
const ARCH_OS_HEIGHT = 40;

const ONLINE_VM_LINE_WIDTH = 120;
const ONLINE_VM_LINE_HEIGHT = 30;

export const PANEL_LABEL_MAPPER = {
  vmpCount: "虚拟化平台数量",
  cloudCount: "云平台数量",
  containerPlatformCount: "容器平台数量",
  physicalCount: "物理平台数量",
  zoneCount: "区域数量",
  clusterCount: "集群数量",
  hostCount: "主机数量",
  vmCount: "虚机数量",
  containerCount: "容器数量",
  tenantCount: "租户数量",
  userCount: "用户数量",
  terminalCount: "终端数量",
  templateInfo: "镜像信息",
  serviceCatalogInfo: "服务目录数量",
  vmTypeStatistics: "云主机/云桌面统计",
  platformTypeStatistics: "虚拟化/云平台虚机统计",
  vmAllocationBar: "虚机分布",
  cpuStatistics: "CPU资源统计",
  memoryStatistics: "内存资源统计",
  storageStatistics: "存储资源统计",
  cpuAllocation: "CPU资源分布",
  memoryAllocation: "内存资源分布",
  storageAllocation: "存储资源分布",
  serverArchPie: "服务器架构统计",
  OSPie: "虚机操作系统统计",
  terminalArchPie: "终端架构统计",
  onlineVmLine: "在线虚机统计",
};

export function renderData(group, id, data, cfg) {
  if (!cfg[group] || !cfg[group].find((v) => v.id === id)) return;
  // 简单的panel，数据就是一个count值
  if (["platformInfo", "resourceInfo", "serviceCatalogInfo"].includes(group)) {
    const target = cfg[group].find((v) => v.id === id);
    target.count = data.count;
    target.label = data.label;
  } else if (group === "templateInfo") {
    const cfgData = cfg[group].find((v) => v.id === id).data;
    const allTemplateTarget = cfgData.find((v) => v.id === "allTemplate");
    allTemplateTarget.count = data.allTemplate.count;
    allTemplateTarget.label = data.allTemplate.label;

    const vmTemplateTarget = cfgData.find((v) => v.id === "vmTemplate");

    vmTemplateTarget.count = data.vmTemplate.count;
    vmTemplateTarget.label = data.vmTemplate.label;

    const containerTemplateTarget = cfgData.find(
      (v) => v.id === "containerTemplate"
    );
    containerTemplateTarget.count = data.containerTemplate.count;
    containerTemplateTarget.label = data.containerTemplate.label;
  } else if (group === "resourceStatisticsInfo") {
    const cfgData = cfg[group].find((v) => v.id === id).data;
    cfgData.values.public.count = data.public.count;
    cfgData.values.public.label = data.public.label;
    cfgData.values.private.count = data.private.count;
    cfgData.values.private.label = data.private.label;
    cfgData.title = data.title;
    cfgData.unit = data.unit;
  } else if (
    [
      "statisticsPieInfo",
      "archOsPieInfo",
      "resourceAllocationInfo",
      "vmAllocationBarInfo",
    ].includes(group)
  ) {
    // pie/bar chart
    cfg[group].find((v) => v.id === id).chartOptions = data;
  }
}

const platformInfoTemp = [
  {
    id: "vmpCount",
    x: 0 * PLATFORM_WIDTH + 10,
    y: 0,
    minWidth: PLATFORM_WIDTH,
    minHeight: PLATFORM_HEIGHT,
    width: PLATFORM_WIDTH,
    height: PLATFORM_HEIGHT,
    img: vmpPlatformIcon,
    label: "虚拟化平台",
    count: 1,
    countColor: "#4276a3",
  },
  {
    id: "cloudCount",
    x: 1 * PLATFORM_WIDTH,
    y: 0,
    minWidth: PLATFORM_WIDTH,
    minHeight: PLATFORM_HEIGHT,
    width: PLATFORM_WIDTH,
    height: PLATFORM_HEIGHT,
    img: cloudPlatformIcon,
    label: "云平台",
    count: 2,
    countColor: "#b7a85f",
  },
  {
    id: "containerPlatformCount",
    x: 2 * PLATFORM_WIDTH,
    y: 0,
    minWidth: PLATFORM_WIDTH,
    minHeight: PLATFORM_HEIGHT,
    width: PLATFORM_WIDTH,
    height: PLATFORM_HEIGHT,
    img: containerPlatformIcon,
    label: "容器平台",
    count: 3,
    countColor: "#2e822f",
  },
  {
    id: "physicalCount",
    x: 3 * PLATFORM_WIDTH,
    y: 0,
    minWidth: PLATFORM_WIDTH,
    minHeight: PLATFORM_HEIGHT,
    width: PLATFORM_WIDTH,
    height: PLATFORM_HEIGHT,
    img: physicalPlatformIcon,
    label: "物理平台",
    count: 4,
    countColor: "#a731ab",
  },
];

const resourceInfoTemp = [
  {
    id: "zoneCount",
    x: 0 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "区域",
    count: 1,
    img: zoneIcon,
  },
  {
    id: "clusterCount",
    x: 1 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "集群",
    count: 2,
    img: clusterIcon,
  },
  {
    id: "hostCount",
    x: 2 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "主机",
    count: 3,
    img: hostIcon,
  },
  {
    id: "vmCount",
    x: 3 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "虚机",
    count: 4,
    img: vmIcon,
  },
  {
    id: "containerCount",
    x: 4 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "容器",
    count: 5,
    img: containerIcon,
  },
  {
    id: "tenantCount",
    x: 5 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "租户",
    count: 6,
    img: tenantIcon,
  },
  {
    id: "userCount",
    x: 6 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "用户",
    count: 7,
    img: userIcon,
  },
  {
    id: "terminalCount",
    x: 7 * RESOURCE_WIDTH,
    y: 1 * PLATFORM_HEIGHT,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "终端",
    count: 8,
    img: terminalIcon,
  },
];

const templateInfoTemp = [
  {
    id: "templateInfo",
    x: 0,
    y: PLATFORM_HEIGHT + RESOURCE_HEIGHT,
    minWidth: TEMPLATE_WIDTH,
    minHeight: TEMPLATE_HEIGHT,
    width: TEMPLATE_WIDTH,
    height: TEMPLATE_HEIGHT,
    data: [
      {
        id: "allTemplate",
        img: allTemplateIcon,
        label: "镜像仓库",
        count: 18,
      },
      {
        id: "vmTemplate",
        img: vmTemplateIcon,
        label: "虚机镜像",
        count: 8,
      },
      {
        id: "containerTemplate",
        img: containerTemplateIcon,
        label: "容器镜像",
        count: 10,
      },
    ],
  },
];

const serviceCatalogInfoTemp = [
  {
    id: "serviceCatalogInfo",
    x: TEMPLATE_WIDTH,
    y: PLATFORM_HEIGHT + RESOURCE_HEIGHT,
    minWidth: SERVICECATALOG_WIDTH,
    minHeight: SERVICECATALOG_HEIGHT,
    width: SERVICECATALOG_WIDTH,
    height: SERVICECATALOG_HEIGHT,
    img: serviceCatalogIcon,
    label: "服务目录",
    count: 11,
  },
];

const resourceStatisticsInfoTemp = [
  {
    id: "cpuStatistics",
    x: 0 * RESOURCE_STATISTICS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT,
    minWidth: RESOURCE_STATISTICS_WIDTH,
    minHeight: RESOURCE_STATISTICS_HEIGHT,
    width: RESOURCE_STATISTICS_WIDTH,
    height: RESOURCE_STATISTICS_HEIGHT,
    data: {
      img: cpuIcon,
      title: "CPU资源统计",
      values: {
        public: {
          count: 1111,
          label: "公有云",
        },
        private: {
          count: 2222,
          label: "私有云",
        },
      },
      unit: "核",
    },
  },
  {
    id: "memoryStatistics",
    x: 1 * RESOURCE_STATISTICS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT,
    minWidth: RESOURCE_STATISTICS_WIDTH,
    minHeight: RESOURCE_STATISTICS_HEIGHT,
    width: RESOURCE_STATISTICS_WIDTH,
    height: RESOURCE_STATISTICS_HEIGHT,
    data: {
      img: memoryIcon,
      title: "内存资源统计",
      values: {
        public: {
          count: 1807.5,
          label: "公有云",
        },
        private: {
          count: 0,
          label: "私有云",
        },
      },
      unit: "GB",
    },
  },
  {
    id: "storageStatistics",
    x: 2 * RESOURCE_STATISTICS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT,
    minWidth: RESOURCE_STATISTICS_WIDTH,
    minHeight: RESOURCE_STATISTICS_HEIGHT,
    width: RESOURCE_STATISTICS_WIDTH,
    height: RESOURCE_STATISTICS_HEIGHT,
    data: {
      img: storageIcon,
      title: "存储资源统计",
      values: {
        public: {
          count: 26.7,
          label: "公有云",
        },
        private: {
          count: 0,
          label: "私有云",
        },
      },
      unit: "GB",
    },
  },
];

const statisticsPieInfoTemp = [
  // 云主机 云桌面比例饼图
  {
    id: "vmTypeStatistics",
    x: 0 * STATISTICSPIE_WIDTH,
    y: PLATFORM_HEIGHT + RESOURCE_HEIGHT + TEMPLATE_HEIGHT,
    minWidth: STATISTICSPIE_WIDTH,
    minHeight: STATISTICSPIE_HEIGHT,
    width: STATISTICSPIE_WIDTH,
    height: STATISTICSPIE_HEIGHT,
    chartOptions: null,
  },
  // 虚拟化 云平台虚机统计
  {
    id: "platformTypeStatistics",
    x: 1 * STATISTICSPIE_WIDTH,
    y: PLATFORM_HEIGHT + RESOURCE_HEIGHT + TEMPLATE_HEIGHT,
    minWidth: STATISTICSPIE_WIDTH,
    minHeight: STATISTICSPIE_HEIGHT,
    width: STATISTICSPIE_WIDTH,
    height: STATISTICSPIE_HEIGHT,
    chartOptions: null,
  },
];

const vmAllocationBarInfoTemp = [
  {
    id: "vmAllocationBar",
    x: 2 * STATISTICSPIE_WIDTH,
    y: PLATFORM_HEIGHT + RESOURCE_HEIGHT + TEMPLATE_HEIGHT,
    minWidth: VM_ALLOCATION_WIDTH,
    minHeight: VM_ALLOCATION_HEIGHT,
    width: VM_ALLOCATION_WIDTH,
    height: VM_ALLOCATION_HEIGHT,
    chartOptions: null,
  },
];

const resourceAllocationInfoTemp = [
  {
    id: "cpuAllocation",
    x: 0 * RESOURCE_ALLOCATION_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT,
    minWidth: RESOURCE_ALLOCATION_WIDTH,
    minHeight: RESOURCE_ALLOCATION_HEIGHT,
    width: RESOURCE_ALLOCATION_WIDTH,
    height: RESOURCE_ALLOCATION_HEIGHT,
    chartOptions: null,
  },
  {
    id: "memoryAllocation",
    x: 1 * RESOURCE_ALLOCATION_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT,
    minWidth: RESOURCE_ALLOCATION_WIDTH,
    minHeight: RESOURCE_ALLOCATION_HEIGHT,
    width: RESOURCE_ALLOCATION_WIDTH,
    height: RESOURCE_ALLOCATION_HEIGHT,
    chartOptions: null,
  },
  {
    id: "storageAllocation",
    x: 2 * RESOURCE_ALLOCATION_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT,
    minWidth: RESOURCE_ALLOCATION_WIDTH,
    minHeight: RESOURCE_ALLOCATION_HEIGHT,
    width: RESOURCE_ALLOCATION_WIDTH,
    height: RESOURCE_ALLOCATION_HEIGHT,
    chartOptions: null,
  },
];

const archOsPieInfoTemp = [
  {
    id: "serverArchPie",
    x: 0 * ARCH_OS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT +
      RESOURCE_ALLOCATION_HEIGHT,
    minWidth: ARCH_OS_WIDTH,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
  {
    id: "OSPie",
    x: 1 * ARCH_OS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT +
      RESOURCE_ALLOCATION_HEIGHT,
    minWidth: ARCH_OS_WIDTH,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
  {
    id: "terminalArchPie",
    x: 2 * ARCH_OS_WIDTH,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT +
      RESOURCE_ALLOCATION_HEIGHT,
    minWidth: ARCH_OS_WIDTH,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
];

const onlineVmLineTemp = [
  {
    id: "onlineVmLine",
    x: 0,
    y:
      PLATFORM_HEIGHT +
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      VM_ALLOCATION_HEIGHT +
      RESOURCE_STATISTICS_HEIGHT +
      RESOURCE_ALLOCATION_HEIGHT +
      ARCH_OS_HEIGHT,
    minWidth: ONLINE_VM_LINE_WIDTH,
    minHeight: ONLINE_VM_LINE_HEIGHT,
    width: ONLINE_VM_LINE_WIDTH,
    height: ONLINE_VM_LINE_HEIGHT,
    chartOptions: {
      id: "onlineVmLineChart",
      template: "LineChart",
      data: null,
    },
  },
];

export const overallDataTemplateBase = {
  platformInfo: platformInfoTemp,
  resourceInfo: resourceInfoTemp,
  templateInfo: templateInfoTemp,
  serviceCatalogInfo: serviceCatalogInfoTemp,
  statisticsPieInfo: statisticsPieInfoTemp,
  vmAllocationBarInfo: vmAllocationBarInfoTemp,
  resourceStatisticsInfo: resourceStatisticsInfoTemp,
  resourceAllocationInfo: resourceAllocationInfoTemp,
  archOsPieInfo: archOsPieInfoTemp,
  onlineVmLine: onlineVmLineTemp,
};

export function extractOverallLocation(overallData) {
  const overallLocation = {};

  Object.keys(overallData).forEach((key) => {
    overallLocation[key] = overallData[key].map((val) => ({
      id: val.id,
      x: val.x,
      y: val.y,
      height: val.height,
      width: val.width,
    }));
  });

  return overallLocation;
}

export function recoverOverallData(overallLocation, overallDataTemplate) {
  const overallData = {};

  Object.keys(overallLocation).forEach((key) => {
    const dataTemplate = overallDataTemplate[key];
    const r = [];
    dataTemplate.forEach((item) => {
      const locationItem = overallLocation[key].find((v) => v.id === item.id);

      if (locationItem) {
        item.x = locationItem.x;
        item.y = locationItem.y;
        item.height = locationItem.height;
        item.width = locationItem.width;
        r.push(item);
      }
    });

    overallData[key] = r;
  });

  return overallData;
}
