export function constructBarData(title, items) {
  return {
    xAxis: {
      type: "category",
      data: items.map((v) => v.name),
      axisLabel: {
        interval: 0,
        rotate: items.length > 4 ? Math.min((items.length - 4) * 5, 40) : 0,
      },
    },
    yAxis: {
      type: "value",
    },
    title: {
      text: title,
      left: "left",
      textStyle: {
        fontWeight: "normal",
      },
      padding: 20,
    },
    tooltip: {
      trigger: "axis",
      axisPointer: {
        type: "shadow",
      },
    },
    grid: {
      bottom: "3%",
      containLabel: true,
    },
    series: [
      {
        data: items.map((v) => ({
          value: v.value,
          itemStyle: {
            color: v.color,
          },
        })),
        type: "bar",
        barMinWidth: 20,
        barMaxWidth: 50,
      },
    ],
  };
}

// eslint-disable-next-line no-unused-vars
export function constructPieData(title, data, innerLabel = true) {
  return {
    title: {
      text: title,
      left: "left",
      textStyle: {
        fontWeight: "normal",
      },
      padding: 20,
    },
    tooltip: {
      trigger: "item",
      formatter: "{b}\n({d}%)",
    },
    series: [
      {
        type: "pie",
        radius: "75%",
        data,
        label: {
          normal: innerLabel
            ? {
                show: true,
                position: "inner",
                formatter: "{b}\n({d}%)",
                fontSize: 14,
                color: "#000",
              }
            : {
                show: true,
                position: "top",
                formatter: "{b}\n({d}%)",
                fontSize: 14,
                color: "#000",
              },
        },
        color: data.map((v) => v.color),
        emphasis: {
          itemStyle: {
            shadowBlur: 10,
            shadowOffsetX: 0,
            shadowColor: "rgba(0, 0, 0, 0.5)",
          },
        },
      },
    ],
  };
}

export const BASIC_COLORS = [
  "#86BBED",
  "#BFB1E0",
  "#7BCDD0",
  "#99E2E6",
  "#5F6190",
];
