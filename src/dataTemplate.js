import clusterIcon from "../src/assets/clusterIcon.png";
import hostIcon from "../src/assets/hostIcon.png";
import vmIcon from "../src/assets/vmIcon.png";
import containerIcon from "../src/assets/containerIcon.png";
import userIcon from "../src/assets/userIcon.png";
import terminalIcon from "../src/assets/terminalIcon.png";
import allTemplateIcon from "../src/assets/allTemplate.svg";
import vmTemplateIcon from "../src/assets/vmTemplate.svg";
import containerTemplateIcon from "../src/assets/containerTemplate.svg";
import serviceCatalogIcon from "../src/assets/serviceCatalogIcon.svg";
import cpuIcon from "../src/assets/cpuIcon.png";
import memoryIcon from "../src/assets/memoryIcon.png";
import storageIcon from "../src/assets/storageIcon.png";

export const MARGIN = 1;

const RESOURCE_WIDTH = 19;
const RESOURCE_HEIGHT = 7;

const TEMPLATE_WIDTH = 89;
const TEMPLATE_HEIGHT = 6;

const SERVICECATALOG_WIDTH = 28;
const SERVICECATALOG_HEIGHT = 6;

const RESOURCE_QUOTA_WIDTH = 29;
const RESOURCE_QUOTA_HEIGHT = 10;

const ARCH_OS_WIDTH = 39;
const ARCH_OS_HEIGHT = 34;

const ONLINE_VM_LINE_WIDTH = 118;
const ONLINE_VM_LINE_HEIGHT = 30;

export const PANEL_LABEL_MAPPER = {
  clusterCount: "集群",
  hostCount: "主机",
  vmCount: "虚机",
  containerCount: "容器",
  userCount: "用户",
  terminalCount: "终端",
  templateInfo: "镜像信息",
  serviceCatalogInfo: "服务目录",
  cpuQuota: "CPU资源配额",
  memoryQuota: "内存资源配额",
  storageQuota: "存储资源配额",
  vmQuota: "虚机配额",
  serverArchPie: "服务器架构统计",
  OSPie: "虚机操作系统统计",
  terminalArchPie: "终端架构统计",
  onlineVmLine: "在线虚机统计",
};

export function renderData(group, id, data, cfg) {
  if (!cfg[group] || !cfg[group].find((v) => v.id === id)) return;
  // 简单的panel，数据就是一个count值
  if (["resourceInfo", "serviceCatalogInfo"].includes(group)) {
    const target = cfg[group].find((v) => v.id === id);
    target.count = data.count;
    target.label = data.label;
  } else if (group === "templateInfo") {
    const cfgData = cfg[group].find((v) => v.id === id).data;
    const allTemplateTarget = cfgData.find((v) => v.id === "allTemplate");
    allTemplateTarget.count = data.allTemplate.count;
    allTemplateTarget.label = data.allTemplate.label;

    const vmTemplateTarget = cfgData.find((v) => v.id === "vmTemplate");

    vmTemplateTarget.count = data.vmTemplate.count;
    vmTemplateTarget.label = data.vmTemplate.label;

    const containerTemplateTarget = cfgData.find(
      (v) => v.id === "containerTemplate"
    );
    containerTemplateTarget.count = data.containerTemplate.count;
    containerTemplateTarget.label = data.containerTemplate.label;
  } else if (group === "resourceQuotaInfo") {
    const cfgData = cfg[group].find((v) => v.id === id).data;
    cfgData.values.used.count = data.used.count;
    cfgData.values.used.label = data.used.label;
    cfgData.values.available.count = data.available.count;
    cfgData.values.available.label = data.available.label;
    cfgData.title = data.title;
    cfgData.unit = data.unit;
  } else if (["archOsPieInfo"].includes(group)) {
    // pie/bar chart
    cfg[group].find((v) => v.id === id).chartOptions = data;
  }
}

const resourceInfoTemp = [
  {
    id: "clusterCount",
    x: 0 * RESOURCE_WIDTH + 1 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "集群",
    count: 2,
    img: clusterIcon,
  },
  {
    id: "hostCount",
    x: 1 * RESOURCE_WIDTH + 2 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "主机",
    count: 3,
    img: hostIcon,
  },
  {
    id: "vmCount",
    x: 2 * RESOURCE_WIDTH + 3 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "虚机",
    count: 4,
    img: vmIcon,
  },
  {
    id: "containerCount",
    x: 3 * RESOURCE_WIDTH + 4 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "容器",
    count: 5,
    img: containerIcon,
  },
  {
    id: "userCount",
    x: 4 * RESOURCE_WIDTH + 5 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH,
    height: RESOURCE_HEIGHT,
    label: "用户",
    count: 7,
    img: userIcon,
  },
  {
    id: "terminalCount",
    x: 5 * RESOURCE_WIDTH + 6 * MARGIN,
    y: 1 * MARGIN,
    minWidth: RESOURCE_WIDTH - 1,
    minHeight: RESOURCE_HEIGHT,
    width: RESOURCE_WIDTH - 1,
    height: RESOURCE_HEIGHT,
    label: "终端",
    count: 8,
    img: terminalIcon,
  },
];

const templateInfoTemp = [
  {
    id: "templateInfo",
    x: MARGIN,
    y: RESOURCE_HEIGHT + 2 * MARGIN,
    minWidth: TEMPLATE_WIDTH,
    minHeight: TEMPLATE_HEIGHT,
    width: TEMPLATE_WIDTH,
    height: TEMPLATE_HEIGHT,
    data: [
      {
        id: "allTemplate",
        img: allTemplateIcon,
        label: "镜像仓库",
        count: 18,
      },
      {
        id: "vmTemplate",
        img: vmTemplateIcon,
        label: "虚机镜像",
        count: 8,
      },
      {
        id: "containerTemplate",
        img: containerTemplateIcon,
        label: "容器镜像",
        count: 10,
      },
    ],
  },
];

const serviceCatalogInfoTemp = [
  {
    id: "serviceCatalogInfo",
    x: TEMPLATE_WIDTH + 2 * MARGIN,
    y: RESOURCE_HEIGHT + 2 * MARGIN,
    minWidth: SERVICECATALOG_WIDTH,
    minHeight: SERVICECATALOG_HEIGHT,
    width: SERVICECATALOG_WIDTH,
    height: SERVICECATALOG_HEIGHT,
    img: serviceCatalogIcon,
    label: "服务目录",
    count: 11,
  },
];

const resourceQuotaInfoTemp = [
  {
    id: "cpuQuota",
    x: 0 * RESOURCE_QUOTA_WIDTH + MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + 3 * MARGIN,
    minWidth: RESOURCE_QUOTA_WIDTH,
    minHeight: RESOURCE_QUOTA_HEIGHT,
    width: RESOURCE_QUOTA_WIDTH,
    height: RESOURCE_QUOTA_HEIGHT,
    data: {
      img: cpuIcon,
      title: "CPU资源配额",
      values: {
        used: {
          count: 1111,
          label: "已分",
        },
        available: {
          count: 2222,
          label: "可用",
        },
      },
      unit: "核",
    },
  },
  {
    id: "memoryQuota",
    x: 1 * RESOURCE_QUOTA_WIDTH + 2 * MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + 3 * MARGIN,
    minWidth: RESOURCE_QUOTA_WIDTH,
    minHeight: RESOURCE_QUOTA_HEIGHT,
    width: RESOURCE_QUOTA_WIDTH,
    height: RESOURCE_QUOTA_HEIGHT,
    data: {
      img: memoryIcon,
      title: "内存资源配额",
      values: {
        used: {
          count: 1807.5,
          label: "已分",
        },
        available: {
          count: 0,
          label: "可用",
        },
      },
      unit: "GB",
    },
  },
  {
    id: "storageQuota",
    x: 2 * RESOURCE_QUOTA_WIDTH + 3 * MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + 3 * MARGIN,
    minWidth: RESOURCE_QUOTA_WIDTH,
    minHeight: RESOURCE_QUOTA_HEIGHT,
    width: RESOURCE_QUOTA_WIDTH,
    height: RESOURCE_QUOTA_HEIGHT,
    data: {
      img: storageIcon,
      title: "存储资源配额",
      values: {
        used: {
          count: 26.7,
          label: "已分",
        },
        available: {
          count: 0,
          label: "可用",
        },
      },
      unit: "GB",
    },
  },
  {
    id: "vmQuota",
    x: 3 * RESOURCE_QUOTA_WIDTH + 4 * MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + 3 * MARGIN,
    minWidth: RESOURCE_QUOTA_WIDTH - 1,
    minHeight: RESOURCE_QUOTA_HEIGHT,
    width: RESOURCE_QUOTA_WIDTH - 1,
    height: RESOURCE_QUOTA_HEIGHT,
    data: {
      img: vmIcon,
      title: "虚机配额",
      values: {
        used: {
          count: 26,
          label: "已分",
        },
        available: {
          count: 0,
          label: "可用",
        },
      },
      unit: "GB",
    },
  },
];

const archOsPieInfoTemp = [
  {
    id: "serverArchPie",
    x: 0 * ARCH_OS_WIDTH + MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + RESOURCE_QUOTA_HEIGHT + 4 * MARGIN,
    minWidth: ARCH_OS_WIDTH,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
  {
    id: "OSPie",
    x: 1 * ARCH_OS_WIDTH + 2 * MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + RESOURCE_QUOTA_HEIGHT + 4 * MARGIN,
    minWidth: ARCH_OS_WIDTH,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
  {
    id: "terminalArchPie",
    x: 2 * ARCH_OS_WIDTH + 3 * MARGIN,
    y: RESOURCE_HEIGHT + TEMPLATE_HEIGHT + RESOURCE_QUOTA_HEIGHT + 4 * MARGIN,
    minWidth: ARCH_OS_WIDTH - 1,
    minHeight: ARCH_OS_HEIGHT,
    width: ARCH_OS_WIDTH - 1,
    height: ARCH_OS_HEIGHT,
    chartOptions: null,
  },
];

const onlineVmLineTemp = [
  {
    id: "onlineVmLine",
    x: MARGIN,
    y:
      RESOURCE_HEIGHT +
      TEMPLATE_HEIGHT +
      RESOURCE_QUOTA_HEIGHT +
      ARCH_OS_HEIGHT +
      5 * MARGIN,
    minWidth: ONLINE_VM_LINE_WIDTH,
    minHeight: ONLINE_VM_LINE_HEIGHT,
    width: ONLINE_VM_LINE_WIDTH,
    height: ONLINE_VM_LINE_HEIGHT,
    chartOptions: {
      id: "onlineVmLineChart",
      template: "LineChart",
      data: null,
    },
  },
];

export const overallDataTemplateBase = {
  resourceInfo: resourceInfoTemp,
  templateInfo: templateInfoTemp,
  serviceCatalogInfo: serviceCatalogInfoTemp,
  resourceQuotaInfo: resourceQuotaInfoTemp,
  archOsPieInfo: archOsPieInfoTemp,
  onlineVmLine: onlineVmLineTemp,
};

export function extractOverallLocation(overallData) {
  const overallLocation = {};

  Object.keys(overallData).forEach((key) => {
    overallLocation[key] = overallData[key].map((val) => ({
      id: val.id,
      x: val.x,
      y: val.y,
      height: val.height,
      width: val.width,
    }));
  });

  return overallLocation;
}

export function recoverOverallData(overallLocation, overallDataTemplate) {
  const overallData = {};

  Object.keys(overallLocation).forEach((key) => {
    const dataTemplate = overallDataTemplate[key];
    if (!dataTemplate) return;
    const r = [];
    dataTemplate.forEach((item) => {
      const locationItem = overallLocation[key].find((v) => v.id === item.id);

      if (locationItem) {
        item.x = locationItem.x;
        item.y = locationItem.y;
        item.height = locationItem.height;
        item.width = locationItem.width;
        r.push(item);
      }
    });

    overallData[key] = r;
  });

  return overallData;
}
